import { Link } from "react-router-dom";
import { AiOutlineShopping } from 'react-icons/ai'
import { useCartContext } from '../../context/CartContext';
import Cart from '../cart';
import './index.css';

const Navbar = () => {
  const { showCart, setShowCart, totalQuantities } = useCartContext()

  return (
    <div className="navbar-container">
      <p className="logo">
        <Link to="/">Home</Link>
      </p>
      <button
        type="button"
        className="cart-icon"
        onClick={() => setShowCart(!showCart)}
      >
        <AiOutlineShopping />
        <span className="cart-item-qty">{totalQuantities}</span>
      </button>

      {showCart && <Cart />}
    </div>
  )
}

export default Navbar
