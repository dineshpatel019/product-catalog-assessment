import { useState } from "react";
import toast from "react-hot-toast";
import { AiOutlineMinus, AiOutlinePlus } from "react-icons/ai";
import { useCartContext } from "../../context/CartContext.js";
import "./index.css";

const GridProduct = ({ product, inventory }) => {
  const { thumbnail, title, price } = product || {};
  const { toogleCartItemQuantity, getProductCartQuantity } = useCartContext();
  const qtyInCart = getProductCartQuantity(product.id);

  const [qty, setQty] = useState(1);

  const incQty = () => {
    setQty((prevQty) => prevQty + 1);
  };

  const decQty = () => {
    setQty((prevQty) => {
      if (prevQty - 1 < 1) {
        return 1;
      }
      return prevQty - 1;
    });
  };

  const addToCart = () => {
    toogleCartItemQuantity(product, qty, "add");
    toast.success(`${qty} ${title} added to cart.`);
    setQty(1);
  };

  return (
    <div className="product-card">
      <img src={thumbnail} width={250} height={250} className="grid-product-image" />
      <div class="product-details">
        <p className="product-name">{title}</p>
        <p className="product-price">${price}</p>
        <div className="warning-container">
          {inventory.stock_quantity < 5 && (
            <p className="stock-warning">
              Hurry up! only {inventory.stock_quantity} items are left!
            </p>
          )}
          {qty + qtyInCart >= inventory.per_order_limit_quantity && (
            <p className="stock-warning">
              {inventory.per_order_limit_quantity} is max purchase quantity for
              product!
            </p>
          )}
        </div>
        <div className="quantity">
          <h3>Quantity:</h3>
          <p className="quantity-desc">
            <button className="minus" onClick={decQty}>
              <AiOutlineMinus />
            </button>
            <span className="num">{qty}</span>
            <button
              className="plus"
              disabled={qty + qtyInCart >= inventory.per_order_limit_quantity}
              onClick={incQty}
            >
              <AiOutlinePlus />
            </button>
          </p>
        </div>

        <button
          class="add-to-cart-button"
          disabled={qtyInCart >= inventory.per_order_limit_quantity}
          onClick={addToCart}
        >
          Add to Cart
        </button>
      </div>
    </div>
  );
};

export default GridProduct;
