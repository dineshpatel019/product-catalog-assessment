import React from "react";
import "react-responsive-modal/styles.css";
import { Modal } from "react-responsive-modal";
import "./index.css";

const CompareProduct = ({ open, category, productToCompare, onCloseModal }) => {
  const formattedCategory = (cat) => {
    const titleCaseWords = cat
      .split("-")
      .map((word) => word.charAt(0).toUpperCase() + word.slice(1));
      
    return titleCaseWords.join(" ");
  };

  return (
    <div>
      <Modal open={open} onClose={onCloseModal} center>
        <div className="compare-products">
          <h2>Compare {formattedCategory(category)}</h2>
          <table>
            <thead>
              <tr>
                <th>#</th>
                <th>Product Name</th>
                <th>Product</th>
                <th>Price</th>
                <th>Discount Price</th>
                <th>Rating</th>
                <th>Brand</th>
                <th>Description</th>
              </tr>
            </thead>
            <tbody>
              {productToCompare.map((product, index) => (
                <tr key={product.id}>
                  <td>{index + 1}</td>
                  <td>{product.title}</td>
                  <td>
                    <img className="product-image" src={product.thumbnail} />
                  </td>
                  <td>${product.price}</td>
                  <td>
                    <b>${product.discountPercentage}</b>
                  </td>
                  <td>{product.rating}</td>
                  <td>{product.brand}</td>
                  <td>{product.description}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </Modal>
    </div>
  );
};

export default CompareProduct;
