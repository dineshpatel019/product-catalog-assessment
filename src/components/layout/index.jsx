import { Outlet } from "react-router-dom";
import { Toaster } from "react-hot-toast";
import { CartContext } from "../../context/CartContext";
import { LayoutContext } from "../../context/LayoutContext";
import Navbar from "../navbar";
import Footer from "../footer";
import "./index.css";

const Layout = () => {
  return (
    <CartContext>
      <LayoutContext>
        <Toaster />
          <div className="layout">
            <header>
              <Navbar />
            </header>
            <main className="main-container">
              <Outlet />
            </main>
            <footer>
              <Footer />
            </footer>
          </div>
      </LayoutContext>
    </CartContext>
  );
};

export default Layout;
