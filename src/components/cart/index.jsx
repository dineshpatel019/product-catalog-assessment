import { useRef, useState } from "react";
import { Link } from "react-router-dom";
import {
  AiOutlineMinus,
  AiOutlinePlus,
  AiOutlineLeft,
  AiOutlineShopping,
} from "react-icons/ai";
import CompareProduct from "../product-compare";
import { TiDeleteOutline } from "react-icons/ti";
import { useCartContext } from "../../context/CartContext";
import "./index.css";

const Cart = () => {
  const cartRef = useRef();
  const {
    totalPrice,
    totalQuantities,
    cartItems,
    setShowCart,
    removeItemFromCart,
    toogleCartItemQuantity,
    productsInCategory,
  } = useCartContext();
  const [open, setOpen] = useState(false);
  const [productToCompare, setProductToCompare] = useState([]);
  const [category, setCompareCategory] = useState("");

  const onCloseModal = () => setOpen(false);

  const compareProduct = (category, catProducts) => {
    setCompareCategory(category);
    setProductToCompare(catProducts);
    setOpen(true);
  };

  return (
    <>
      <div className="cart-wrapper" ref={cartRef}>
        <div className="cart-container">
          <button
            type="button"
            className="cart-heading"
            onClick={() => setShowCart(false)}
          >
            <AiOutlineLeft />
            <span className="heading">Your Cart</span>
            <span className="cart-num-items">({totalQuantities})</span>
          </button>

          <div className="cart-product-container">
            {cartItems.map((product) => {
              const categoryProducts = productsInCategory(product.category);
              return (
                <div key={product.id} className="cart-item">
                  <img className="product-image" src={product.thumbnail}></img>
                  <div className="cart-product-details">
                    <h3 className="product-name">{product.title}</h3>
                    <p className="product-price">
                      ${product.price} &#x2715; {product.quantity} = $
                      {product.price * product.quantity}
                    </p>
                    <p className="quantity-desc">
                      <span
                        className="minus"
                        onClick={() =>
                          product?.quantity > 1 &&
                          toogleCartItemQuantity(product, 1, "remove")
                        }
                      >
                        <AiOutlineMinus />
                      </span>
                      <span className="num">{product.quantity}</span>
                      <span
                        className="plus"
                        onClick={() =>
                          toogleCartItemQuantity(product, 1, "add")
                        }
                      >
                        <AiOutlinePlus />
                      </span>
                    </p>
                    {categoryProducts.length > 1 && (
                      <button
                        type="button"
                        onClick={() =>
                          compareProduct(product.category, categoryProducts)
                        }
                        className="btn btn-product-compare"
                      >
                        Compare Products
                      </button>
                    )}
                    <button
                      type="button"
                      className="remove-item"
                      onClick={() => removeItemFromCart(product.id)}
                    >
                      <TiDeleteOutline />
                    </button>
                  </div>
                </div>
              );
            })}
          </div>

          {cartItems.length === 0 && (
            <div className="empty-cart">
              <AiOutlineShopping size={150} />
              <h3>Your shopping bag is empty</h3>
              <Link to="/">
                <button
                  type="button"
                  onClick={() => setShowCart(false)}
                  className="btn"
                >
                  Continue Shopping
                </button>
              </Link>
            </div>
          )}

          {cartItems.length >= 1 && (
            <div className="cart-action-container">
              <div className="total">
                <h3>Subtotal:</h3>
                <h3>${totalPrice}</h3>
              </div>
              <div className="btn-container">
                <button className="btn">Submit Payment</button>
              </div>
            </div>
          )}
        </div>
      </div>
      <CompareProduct
        open={open}
        category={category}
        productToCompare={productToCompare}
        onCloseModal={onCloseModal}
      />
    </>
  );
};

export default Cart;
