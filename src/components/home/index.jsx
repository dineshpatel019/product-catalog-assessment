import React from "react";
import { useLayoutContext } from "../../context/LayoutContext";
import GridProduct from "../grid-product";
import TableProduct from "../table-product";
import products from "../../utils/products.json";
import productInventory from '../../utils/inventory.json';
import "./index.css";

const Home = () => {
  const { layout, handleLayoutChange } = useLayoutContext();
  
  const inventoryMap = {};
  productInventory.forEach((product) => {
    inventoryMap[product.id] = product;
  });

  return (
    <div>
      <div className="products-heading">
        <h2>Best Selling Products</h2>
        <p>Watches, bags, and sunglasses of numerous variations</p>
      </div>

      <div className="view-select">
        <label htmlFor="view">Select View: </label>
        <select id="view" value={layout} onChange={handleLayoutChange}>
          <option value="grid">Grid View</option>
          <option value="table">Table View</option>
        </select>
      </div>

      {layout === "grid" ? (
        <div className="grid-products-container">
          {products?.map((product) => (
            <GridProduct key={product.id} product={product} inventory={inventoryMap[product.id]} />
          ))}
        </div>
      ) : (
        <div className="table-products-container">
          <table className="table-view">
            <thead>
              <tr>
                <th>ID</th>
                <th>Product</th>
                <th>Brand</th>
                <th>Rating</th>
                <th>Price</th>
                <th>Quantity</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {products?.map((product) => (
                <TableProduct key={product.id} product={product} inventory={inventoryMap[product.id]} />
              ))}
            </tbody>
          </table>
        </div>
      )}
    </div>
  );
};

export default Home;
