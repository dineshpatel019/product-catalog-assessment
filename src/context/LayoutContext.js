import { createContext, useContext, useState, useEffect } from "react";
const Context = createContext();

export const LayoutContext = ({ children }) => {
  const initialLayout =
    JSON.parse(localStorage.getItem("selected-layout")) || 'grid';
  const [layout, setLayout] = useState(initialLayout);

  useEffect(() => {
    localStorage.setItem("selected-layout", JSON.stringify(layout));
  }, [layout]);

  const handleLayoutChange = (e) => {
    setLayout(e.target.value);
  };

  return (
    <Context.Provider
      value={{
        layout,
        handleLayoutChange,
      }}
    >
      {children}
    </Context.Provider>
  );
};

export const useLayoutContext = () => useContext(Context);
