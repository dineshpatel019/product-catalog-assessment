import { createContext, useContext, useState, useEffect } from "react";
const Context = createContext();

export const CartContext = ({ children }) => {
  const initialCartItems = JSON.parse(localStorage.getItem("cartItems")) || [];
  const initialTotalPrice = JSON.parse(localStorage.getItem("totalPrice")) || 0;
  const initialTotalQuantities =
    JSON.parse(localStorage.getItem("totalQuantities")) || 0;

  const [showCart, setShowCart] = useState(false);
  const [cartItems, setCartItems] = useState(initialCartItems);
  const [totalPrice, setTotalPrice] = useState(initialTotalPrice);
  const [totalQuantities, setTotalQuantities] = useState(initialTotalQuantities);

  useEffect(() => {
    localStorage.setItem('cartItems', JSON.stringify(cartItems));
    localStorage.setItem('totalPrice', JSON.stringify(totalPrice));
    localStorage.setItem('totalQuantities', JSON.stringify(totalQuantities));
  }, [cartItems, totalPrice, totalQuantities]);

  const removeItemFromCart = (productId) => {
    const product = cartItems.find((item) => item.id == productId);
    if (!product) return;

    const filteredProducts = cartItems.filter((item) => item.id !== productId);
    setCartItems([...filteredProducts]);

    setTotalQuantities((prevTotalQuantity) =>
      prevTotalQuantity > 0 ? prevTotalQuantity - product.quantity : 0
    );
    setTotalPrice((prevTotalPrice) =>
      prevTotalPrice > 0 ? prevTotalPrice - product.price * product.quantity : 0
    );
  };

  const toogleCartItemQuantity = (
    product,
    quantity,
    action = "add",
    eventFromBuyNow = false
  ) => {
    if (action === "add") {
      let productIndex = cartItems.findIndex(
        (item) => item.id === product.id
      );

      if (eventFromBuyNow && productIndex !== -1) {
        return;
      }

      setTotalPrice(
        (prevTotalPrice) => prevTotalPrice + product.price * quantity
      );
      setTotalQuantities((prevTotalQuantity) => prevTotalQuantity + quantity);

      if (productIndex !== -1) {
        const newItems = [...cartItems];
        newItems[productIndex].quantity =
          newItems[productIndex].quantity + quantity;
        setCartItems(newItems);
      } else {
        product.quantity = quantity;
        setCartItems([...cartItems, { ...product }]);
      }
    } else if (action === "remove") {
      let productIndex = cartItems.findIndex(
        (item) => item.id === product.id
      );
      if (productIndex == -1) {
        return;
      }

      setTotalPrice(
        (prevTotalPrice) => prevTotalPrice - product.price * quantity
      );
      setTotalQuantities((prevTotalQuantity) => prevTotalQuantity - quantity);

      const newItems = [...cartItems];
      newItems[productIndex].quantity =
        newItems[productIndex].quantity - quantity;
      setCartItems(newItems);
    }
  };

  const getProductCartQuantity = (productId) => {
    let productIndex = cartItems.findIndex(
      (item) => item.id === productId
    );

    return productIndex !== -1 ? cartItems[productIndex].quantity : 0;
  }

  const productsInCategory = (category) => {
    return cartItems.filter((item) => item.category === category);
  };

  return (
    <Context.Provider
      value={{
        setCartItems,
        setTotalPrice,
        setTotalQuantities,
        showCart,
        setShowCart,
        cartItems,
        totalPrice,
        totalQuantities,
        toogleCartItemQuantity,
        removeItemFromCart,
        getProductCartQuantity,
        productsInCategory
      }}
    >
      {children}
    </Context.Provider>
  );
};

export const useCartContext = () => useContext(Context);
